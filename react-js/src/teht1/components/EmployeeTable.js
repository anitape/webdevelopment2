import React, { useState, useEffect } from 'react';
import axios from 'axios';
import EmployeeRow from './EmployeeRow';

const EmployeeTable = () => {

    const [employees, setEmployees] = useState([]);
    const employeeTable = [];
    const getEmployees = () => {
        axios.get('http://localhost:3001/tyontekijat')
            .then(response => {
                console.log('promise fulfilled');
                console.log(response.data);
                setEmployees(response.data);
            })
    };

    useEffect(getEmployees, []);

    employees.map((employee) => {
        employeeTable.push(<EmployeeRow key={employee.id} employee={employee} phone={employee.phone} />);
    }
        );

    return(
        <div>
            <table><tbody>
            <tr><th>Etunimi</th><th>Sukunimi</th><th>Osoite</th><th>Puhelin1</th><th>Puhelin2</th></tr>
            {employeeTable}
            </tbody></table>
        </div>

    )
};

export default EmployeeTable;