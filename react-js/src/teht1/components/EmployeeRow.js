import React from 'react';

const EmployeeRow = (props) => {
    return(
        <tr>
            <td>{props.employee.firstName}</td>
            <td>{props.employee.lastName}</td>
            <td>{props.employee.address}</td>
            <td>{props.phone[0].phone1}</td>
            <td>{props.phone[1].phone2}</td>
        </tr>
    )
};

export default EmployeeRow;