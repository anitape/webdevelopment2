import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import FillariTable from "./components/FIllariTable";
import FillariMap from "./components/FillariMap";
import axios from "axios";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Weather from "./components/Weather";

function App() {
    const [asemat, setAsemat] = useState([]);
    const [weather, setWeather] = useState([]);
    const [icon, setIcon] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            axios.get("https://opendata.arcgis.com/datasets/ee54ddf0b4db41d69410689394b7c00d_0.geojson")
                .then(response => {
                    setAsemat(response.data.features);
                    console.log(response.data.features);
                });
        };
        fetchData();
    }, []);

    useEffect(() => {
        const fetchWeather = async () => {
            axios.get("http://api.openweathermap.org/data/2.5/weather?q=Helsinki&lang=fi&appid=a75d694fdd729e9531b36fc71d2fa1b3")
                .then(response => {
                    setWeather(response.data);
                    console.log(response.data);
                    setIcon("http://openweathermap.org/img/wn/" + response.data.weather[0].icon + "@2x.png");
                    console.log("Icon ", icon);
                })
        };
        fetchWeather();
    }, []);

    const [searchText, setSearchText] = useState('');

    const searchTextChanged = (searchMe) => {
        setSearchText(searchMe);
        //console.log('This is searchMe of searchTextChanged', searchText)
    };

    const stChanged = (event) => {
        searchTextChanged(event.target.value);
        //console.log('stChanged Input value is ', event.target.value)
    };

  return (
     <div>
        {!asemat.length && !weather.length ? <div>Ladataan tiedot...</div> :
            <div>
                <FillariMap asemat={asemat} searchText={searchText} />
                    <div className="main-container">
                       <Row>
                           <Col md={8}>
                               <h1>FillariAPI</h1>
                               <br/>
                               <Row>
                                   <Col sm="5">
                                       <Form.Control type='search' placeholder='Etsi kaupunkipyöräasema..' onChange={stChanged} /><br/>
                                   </Col>
                               </Row>
                               <FillariTable asemat={asemat} searchText={searchText}/>
                           </Col>
                           <Col md={4}>
                               <Weather weather={weather} icon={icon}/>
                           </Col>
                       </Row>
                    </div>
            </div>
         }
     </div>
  )
}


export default App;
/**

 */