import React from 'react';
import FillariRow from './FillariRow';
import Table from 'react-bootstrap/Table';

const FillariTable = (props) => {
    const asemat = props.asemat;
    const asemaTaulukko = [];

    asemat.map((asema) => {
        asemaTaulukko.push((asema.properties.Nimi.search(props.searchText) < 0 &&
            asema.properties.Nimi.search(props.searchText.toUpperCase()) < 0 &&
            asema.properties.Kaupunki.search(props.searchText) < 0) ||
            <FillariRow key={asema.properties.FID} asema={asema}/>)});

    return(
        <Table bordered size="sm">
            <thead>
                  <tr>
                      <th>Nro</th>
                      <th>Nimi</th>
                      <th>Osoite</th>
                      <th>Kaupunki</th>
                      <th>Kapasiteetti</th>
                      <th>Leveysaste</th>
                      <th>Pituusaste</th>
                  </tr>
            </thead>
             <tbody>
                   {asemaTaulukko}
             </tbody>
        </Table>
    )
};

export default FillariTable;
