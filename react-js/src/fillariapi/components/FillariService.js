import axios from 'axios';
const baseUrl = 'https://opendata.arcgis.com/datasets/ee54ddf0b4db41d69410689394b7c00d_0.geojson';

const getAll = () => {
    const request = axios.get(baseUrl);
    return request.then(response => response.data)
};

export default {
    getAll: getAll,
    getIcon: getIcon
}