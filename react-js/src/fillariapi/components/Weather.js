import React from 'react';
import Table from "react-bootstrap/Table";

const Weather = (props) => {
    const weather = props.weather;
    const icon = props.icon;
    let date = new Date();

    return(
        <div>
            <div className="weatherTable">
                <h3>Säätila {weather.name}</h3>
                <h2 className="nobreakline">{(weather.main.temp - 273.15).toFixed(1)} ºC</h2>
                {weather.weather[0].description}
                <img src={icon}/>
                <br/>
                <br/>
                Haettu {date.getDate()}.{date.getMonth()+1}.{date.getFullYear()}
                &nbsp; klo {date.getHours()}:{date.getMinutes()}:{date.getSeconds()}
                <br/><br/>
                <Table>
                    <tbody>
                        <tr>
                            <td>Tuulen nopeus</td>
                            <td>{weather.wind.speed} m/sek</td>
                        </tr>
                        <tr>
                            <td>Ilman kosteus</td>
                            <td>{weather.main.humidity} %</td>
                        </tr>
                        <tr>
                            <td>Pilvisyys</td>
                            <td>{weather.clouds.all} %</td>
                        </tr>
                        <tr>
                            <td>Ilmanpaine</td>
                            <td>{weather.main.pressure} hPa</td>
                        </tr>
                         <tr>
                            <td>Auringonnousu</td>
                            <td>{new Date(weather.sys.sunrise*1000).toTimeString()}</td>
                        </tr>
                        <tr>
                            <td>Auringonlasku</td>
                            <td>{new Date(weather.sys.sunset*1000).toTimeString()}</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </div>
    )
};

export default Weather;
//