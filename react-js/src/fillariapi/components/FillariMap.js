import React from 'react';
import "../styles.css";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";


const FillariMap = (props) => {
    const asemat = props.asemat;
    const locations = [];
    const DEFAULT_LATITUDE = 60.166640739;
    const DEFAULT_LANGITUDE = 24.943536799;

    asemat.map(coords => {
        locations.push((coords.properties.Nimi.search(props.searchText) < 0 && coords.properties.Kaupunki.search(props.searchText) < 0) ||
            <Marker key={coords.properties.FID} position={[coords.geometry.coordinates[1], coords.geometry.coordinates[0]]}>
                <Popup>{coords.properties.Osoite}, {coords.properties.Kaupunki}</Popup>
            </Marker>)
    });

    return(
        <div>
            <MapContainer center={[DEFAULT_LATITUDE, DEFAULT_LANGITUDE]} zoom={13}>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attrubution='&copy; <a href="http://osm.org/copyright">
                    OpenStreetMap</a> contributors ' />
                {locations}
            </MapContainer>
            <br/>
        </div>
    )
};

export default FillariMap;



