import React from 'react';

const FillariRow = (props) => {
    return(
        <tr>
            <td>{props.asema.properties.FID}</td>
            <td>{props.asema.properties.Nimi}</td>
            <td>{props.asema.properties.Osoite}</td>
            <td>{props.asema.properties.Kaupunki}</td>
            <td>{props.asema.properties.Kapasiteet}</td>
            <td>{props.asema.geometry.coordinates[1].toFixed(4)}</td>
            <td>{props.asema.geometry.coordinates[0].toFixed(4)}</td>
        </tr>
    )
};

export default FillariRow;