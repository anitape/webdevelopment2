
var mymap = L.map('mapid').setView([60.166640739, 24.94], 13);
L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a','b','c']
}).addTo( mymap );


const startingCoords = {
    lat: 60.166640739,
    lng: 24.943536799
};

console.log("startingCoords.lat ", startingCoords.lat);
console.log("startingCoords.lng", startingCoords.lng);

const endingCoords = {
    lat: 60.206376371,
    lng: 24.656728549
};


L.Routing.control({
    waypoints: [L.latLng(startingCoords.lat, startingCoords.lng), L.latLng(endingCoords.lat, endingCoords.lng)]
}).addTo(mymap);
