import React, {useEffect, useState} from 'react'
import {
    BrowserRouter as Router,
    Switch, Route, Link
} from 'react-router-dom'

import Add from './components/Add'
import Home from './components/Home'
import List from './components/List'
import axios from "axios";

/*const Notes = () => {

    return(
        <div>
            <p>Valinta 1.</p>
        </div>
    )
} */

/*const Users = () => {

    return(
        <div>
            <p>Valinta 2.</p>
        </div>
    )
}*/

/*const Home = () => {
    return(
        <div>
            <p>Koti</p>
        </div>
    )
}*/

const App = () => {
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(true);
    const [added, setAdded] = useState(false);

    const addedDone = () => {
        setAdded(!added);
    };

    useEffect(() => {
        const fetchData = async () => {
            axios.get("http://localhost:3001/events")
                .then(response => {
                    setList(response.data);
                    console.log("Result data added", response.data);
                })
        };
        fetchData();
        setLoading(false);
        console.log("Loading changed to ", loading);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            axios.get("http://localhost:3001/events")
                .then(response => {
                    setList(response.data);
                    console.log("Result data added", response.data);
                })
        };
        fetchData();
        setLoading(false);
        console.log("Loading changed to ", loading);
    }, [added]);

    const padding = {
        padding: 5
    };

    return (
        <div className="container">
            <Router>
                <div>
                    <Link style={padding} to="/">home</Link>
                    <Link style={padding} to="/add">add</Link>
                    <Link style={padding} to="/list">list</Link>
                </div>

                <Switch>
                    <Route path="/add">
                        <Add list={list} added={added} addedDone={addedDone}/>
                    </Route>
                    <Route path="/list" >
                        <List list={list} loading={loading}/>
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>

                <div>
                    <i>Esimerkkivalikko </i>
                    <i>perustuu HY:n fullstackopen-kurssimateriaaliin</i>
                </div>
            </Router>
        </div>
    )
}

export default App
