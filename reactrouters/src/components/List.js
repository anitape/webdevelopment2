import React from "react";
import { Table } from 'react-bootstrap';

const List = (props) => {
    const loading = props.loading;
    const list = props.list;
    return(
        <div className="container">
            {/* A JSX comment */}

            {/* <div>
                <p>Valinta 2.</p>
            </div>
            */}
            {loading ? <div>loading...</div> :
            <Table striped>
            <tbody>
            {list.map((item) =>
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.city}</td>
                    <td>{item.date}</td>
                </tr>
            )}
            </tbody>
            </Table>}
        </div>
    )
};

export default List