import axios from 'axios';
const baseUrl = 'http://localhost:3001/events';

const getAll = () => {
    const request = axios.get(baseUrl);
    return request.then(response => response.data)
};

const create = newEvent => {
    const request = axios.post(baseUrl, newEvent);
    return request.then(response => response.data)
};

export default {
    getAll: getAll,
    create: create
}