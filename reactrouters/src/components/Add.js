import React, {useEffect, useRef, useState} from 'react'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import noteService from "./Service";

const Add = (props) => {
    const list = props.list;
    const [events, setEvents] = useState([]);
    const [newEvent, setNewEvent] = useState({ id: '', name: '', city: '', date: ''});
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');
    const textInput = useRef(null);
    const [pressed, setPressed] = useState(false);

    const isAdded = () => {
        props.addedDone();
    };

    const handleNoteChange = event => {
        newEvent.id = list.length + 1;
        const { name, value } = event.target;
        setNewEvent(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const checkForm = () => {
        if((!newEvent.name || !newEvent.city || !newEvent.date) && !pressed) {
            setError("Täytä kaikki kentät!");
            setSuccess("");
        }
        else if ((newEvent.name && newEvent.city && newEvent.date) && !pressed) {
            setSuccess("Kaikki kentät on täytetty. Lomake on valmis lähetettäväksi.");
            setError("");
        }
        else if (pressed) {
            setError("");
            setSuccess("Tapahtuma on lisätty onnistuneesti!");
            setPressed(false);
        }
    };

    useEffect(() => {
        checkForm();
        }, [newEvent]);

    const addEvent = event => {
        if (newEvent.name && newEvent.city && newEvent.date) {
            event.preventDefault();
            noteService
                .create(newEvent)
                .then(returnedNote => {
                    setEvents(events.concat(returnedNote));
                    setEvents('')
                });
            isAdded();
            event.target.reset();
            textInput.current.focus();
            setNewEvent({id: '', name: '', city: '', date: ''});
            setPressed(true);
        }
        event.preventDefault();
    };

    return(
        <div className="container">
            <Container>
                <Row>
                    <Col md={{span: 8, offset: 2}}>
                        <h1>Lisää uusi tapahtuma</h1>
                        <div>{error} {success}</div>
                        <Form onSubmit={addEvent}>
                            <Form.Label>Nimi</Form.Label>
                            <Form.Control ref={textInput} name="name" type="text" placeholder="Nimi" onInput={handleNoteChange} />
                            <Form.Label>Kaupunki</Form.Label>
                            <Form.Control name="city" type="text" placeholder="Kaupunki" onInput={handleNoteChange}/>
                            <Form.Label>Päivämäärä</Form.Label>
                            <Form.Control name="date" type="text" placeholder="Päivämäärä" onInput={handleNoteChange}/>
                            <br />
                            <Button variant="primary" type="submit">Lisää</Button>
                        </Form>
                    </Col>
                  </Row>
            </Container>
        </div>
    )
}

export default Add